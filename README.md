# NYT Data Aggregator

## Prerequisites
- Git for Windows install
- Python install
- Visual Studio Code (Recommended IDE)
  - Python extension (Recommended)
  - REST client extension (Recommended)
- Sourcetree Git GUI

## Getting started
### Cloning repository
1. Install Git for Windows.
2. On GitLab Project dashboard click *Code -> Clone with HTTPS* and copy that URL.
3. In SourceTree click on *Clone* tab
   1. Source Path / URL -> paste copied GitLab URL.
   2. Destination Path  -> any folder on your machine.
   3. Click *Clone* button.
   4. At this point, you should have local repository copied in Sourcetree and ready to go.

### Preparing local dev environment
1. Install Python.
2. Install Visual Studio Code with recommended extensions.
3. Open repository folder in VS Code
4. In VS Code top menu *Terminal -> New Terminal*
5. In opened terminal, run following commands:
   1. cd server (this will change active work directory to \nyt-data-aggregator\server - where code is)
   2. pip install -r requirements.txt (this will install all external package dependencies)
   3. python app.py (this will run local python application locally)
6. Navigate to *services/example.http* and open that file
7. Click *Send request* on any example request
