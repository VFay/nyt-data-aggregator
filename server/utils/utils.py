import configparser

def read_config():
    config = configparser.ConfigParser()
    config.read('config.ini')

    return config

def nyt_apy_key(): 
    return read_config()['NYT']['api_key']
      
def get_review_url():
    return read_config()['NYT']['review_url']

def get_top_stories_url():
    return read_config()['NYT']['top_stories_url']