import requests
from utils import utils
from constants import http_status_codes

book_titles = ["1Q84", "The Great Gatsby", "1984"]

def get_nyt_book_reviews():
    reviews = {}

    for title in book_titles: 
        params = {
            'title': title, 
            'api-key': utils.nyt_apy_key() 
        }

        response = requests.get(utils.get_review_url(), params=params)

        if response.status_code == http_status_codes.HTTP_SUCCESS_CODE:
            reviews[title] = response.json().get('results', [])
        else:
            reviews[title] = f"Error {response.status_code}: Unable to fetch reviews."

    return reviews