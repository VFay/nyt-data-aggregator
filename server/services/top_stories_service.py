import requests
from utils import utils

top_stories_allowed_values = ["arts", "automobiles", "business", "fashion", "food"]

def get_nyt_top_stories(section):
    if section not in top_stories_allowed_values:
        return None

    params = {
        'api-key': utils.nyt_apy_key() 
    }

    try:
        response = requests.get(build_url(section), params=params)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException:
        return None
    
def build_url(section):
    return utils.get_top_stories_url().replace("{section}", section)