from flask import Flask, abort, request

from services.book_review_service import get_nyt_book_reviews
from services.top_stories_service import get_nyt_top_stories

from constants import http_status_codes

app = Flask(__name__)

@app.route('/')
def home():
    return "The New York Times Data Aggregator", http_status_codes.HTTP_SUCCESS_CODE

@app.route('/book_reviews', methods=['POST'])
def get_book_reviews():
    return get_nyt_book_reviews()

@app.route('/top_stories', methods=['POST'])
def get_top_stories_by_section():
    stories = get_nyt_top_stories(request.json['section'])

    if stories is None:
        return abort(http_status_codes.HTTP_NOT_FOUND_CODE)

    return stories

app.run(debug=True)